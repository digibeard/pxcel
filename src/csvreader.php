<?php
require_once ('./config.php');
$csvFile = './dataset.csv';
if (!file_exists($csvFile)) {
    die("File not found: $csvFile");
}
$file = fopen($csvFile, 'r');
if ($file === false) {
    die("Error opening file: $csvFile");
}

while (($data = fgetcsv($file)) !== false) {

    $conn = new \mysqli(MYSQL_ROOT_HOST, MYSQL_USER, MYSQL_USER, MYSQL_DATABASE);
    $stmt = $conn->prepare("INSERT INTO clients (category, firstname, lastname, email, gender, birthDate) VALUES (?, ?, ? , ? , ? , ?)");
    $stmt->bind_param("ssssss", $data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);

    if ($stmt->execute()) {
        echo "Data inserted successfully";
    } else {
        echo "Error: " . $stmt->error;
    }
}

$stmt->close();
$conn->close();

fclose($file);


