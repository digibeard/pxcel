<?php

require_once('MySQLConnection.php');
$connection = new MySQLConnection();

require_once ('query_builder.php');
require_once ('config.php');

$items = $connection(getQueryForPagedRecords($_GET));
$count = $connection(getQueryForCountReacordsQuery($_GET));
$page = 1;
if (array_key_exists('page', $_GET)) {$page = $_GET['page'];}
$totalRecords = $count[0]['total'];
$pages = ceil($totalRecords/ITEMS_PER_PAGE);

$response = array(
    'success' => true,
    'message' => 'Data processed successfully',
    'page' => $page,
    'pages' => $pages,
    'items' => $items
);
header('Content-Type: application/json');
echo json_encode($response);
