<?php
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="clients.csv"');

require_once('MySQLConnection.php');
$connection = new MySQLConnection();

require_once ('query_builder.php');
require_once ('config.php');

$data = $connection->fetchRecordsWithHeader(getQueryForAllRecords($_GET));

$csvContent = implode(PHP_EOL, array_map(function ($row) {
    return implode(',', $row);
}, $data));

echo $csvContent;