<?php

require_once('config.php');

class MySQLConnection
{
    private $connection;

    public function __construct()
    {
        $this->connection = new \mysqli(MYSQL_ROOT_HOST, MYSQL_USER, MYSQL_USER, MYSQL_DATABASE);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    public function fetch($sql)
    {
        $iterator = $this->connection->query($sql);
        $rez = [];
        while ($row = $iterator->fetch_assoc()) {
            $rez[] = $row;
        }
        return $rez;
    }
    public function __invoke($sql)
    {
        return $this->fetch($sql);
    }

    public function fetchRecordsWithHeader($sql)
    {
        $iterator = $this->connection->query($sql);
        $rez = [[
            "category" => "category",
            "firstname" => "firstname",
            "lastname" => "lasttname",
            "email" => "email",
            "gender" => "gender",
            "birth_date" => "birth_date",
            "age" => "age",
        ]];
        while ($row = $iterator->fetch_assoc()) {
            $rez[] = $row;
        }
        return $rez;
    }

    public function query($sql)
    {
        return $this->connection->query($sql);
    }

    public function execute($sql)
    {
        if ($this->connection->query($sql) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function close()
    {
        $this->connection->close();
    }

    public function __destruct()
    {
        $this->connection->close();
    }
}