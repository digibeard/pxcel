<?php
require_once ('config.php');
$example = array(
    'ageHigh' => 45,
    'category' => 'clothes',
    'ageLow' => 20,
);
const fielfsToFilter = ['category', 'gender', 'birthDate', 'age', "ageLow", "ageHigh"];
function sqlQueryFragmentFromParams(array $rawParams)
{
    $params = [];
    foreach ($rawParams as $x => $val) {
        if (in_array($x, fielfsToFilter))
            $params[$x] = htmlspecialchars($rawParams[$x], ENT_QUOTES, 'UTF-8');
    }

    $ageCondition='';
    if (array_key_exists('ageLow', $params) && array_key_exists('ageHigh', $params)) {
        unset($params['age']);
        $ageCondition = " HAVING age > $params[ageLow] AND age < $params[ageHigh] ";
    }
    if (array_key_exists('age', $params)) {
        $ageCondition = "  HAVING age = $params[age] ";
        unset($params['age']);
    }
    unset($params['ageLow']);
    unset($params['ageHigh']);

    $eqCondition = ' WHERE ';
    foreach ($params as $x => $val) {
        $eqCondition .= " $x = '$val' AND ";
    }

    $query = ' FROM clients ' . $eqCondition . 'TRUE' . $ageCondition;
    return $query;
}

function getQueryForPagedRecords($rawParams)
{
    $page = 1;
    if (array_key_exists('page', $rawParams)) {
        $page = $rawParams['page'];
        unset($rawParams['page']);
    }
    $query = 'SELECT category, id, firstname, lastname, email, gender, birth_date,  TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age' . sqlQueryFragmentFromParams($rawParams) . ' LIMIT '.strval(ITEMS_PER_PAGE).' OFFSET '.strval(($page-1)*ITEMS_PER_PAGE);
    return $query;
}

function getQueryForAllRecords($rawParams)
{
    $query = 'SELECT category, firstname, lastname, email, gender, birth_date,  TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age' . sqlQueryFragmentFromParams($rawParams);
    return $query;
}

function getQueryForCountReacordsQuery($rawParams)
{
    return 'SELECT COUNT(*) as total FROM ( SELECT TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age ' . sqlQueryFragmentFromParams($rawParams) . ') AS clients2 ';
}

//echo getQueryForPagedRecords($example);
