<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pxcel clients table</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
    <script src="https://cdn.jsdelivr.net/npm/alpinejs@2.8.2/dist/alpine.min.js"></script>
</head>

<body>
<section class="section" x-data="{
message: 'pxcel',items:[], ageRange: false, page:1, pages: 1,
category: null, gender: null, age: null, ageLow: null, ageHigh:null , birth_date:null,
filter: {}, oldFilter:{}, csv: 'csv.php',
initPxcel: function (){
this.fetchData();
},
apiUrl: function (filter) {return 'api.php?'+(new URLSearchParams(Object.entries(filter)).toString()) },
fetchData: async function (url='api.php?page=1') {
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    console.log(data.items);
    this.items = data.items;
    this.pages = data.pages;
  } catch (error) {
    console.error('Error:', error.message);
  }
},
changePage: function (){
     let url = this.apiUrl({...this.filter,page:this.page})
     this.fetchData(url);
     console.log('lets go')
},
applyFilters: function (){
     this.filter = {};
     if (this.category) this.filter['category']=this.category;
     if (this.gender) this.filter['gender']=this.gender;
     if (!this.ageRange && this.age) this.filter['age']=this.age;
     if (this.birth_date) this.filter['birth_date']=this.birth_date;
     if (this.ageRange && this.ageLow && this.ageHigh) {
         this.filter['ageLow']=this.ageLow;
         this.filter['ageHigh']=this.ageHigh;
     }
     if (this.apiUrl(this.filter) !== this.apiUrl(this.oldFilter)) this.page=1;
     let url = this.apiUrl({...this.filter,page:this.page})
     this.fetchData(url);
     this.oldFilter = this.filter;
     this.csv = 'csv.php?'+(new URLSearchParams(Object.entries(this.filter)).toString())
  }
}" x-init="initPxcel">

    <div class="container">
        <nav class="pagination is-centered" role="navigation" aria-label="pagination">
            <a x-on:click.prevent="page--; changePage()"  class="pagination-previous" title="Previous">Previous</a>
            <a x-on:click.prevent="page++; changePage()" class="pagination-next" title="Next">Next</a>
            <ul class="pagination-list">
                <li><a x-on:click.prevent="page=1; changePage()"  class="pagination-link" aria-label="Goto page 1">1</a></li>
                <li><span class="pagination-ellipsis">&hellip;</span></li>
                <li><a x-text="page" class="pagination-link is-current" aria-current="page">4</a></li>
                <li><span class="pagination-ellipsis">&hellip;</span></li>
                <li><a x-on:click.prevent="page=pages; changePage()"  x-text="pages" class="pagination-link">8</a></li>
            </ul>
        </nav>
        <div class="has-text-centered">
            <button x-on:click="applyFilters" class="button is-primary">filter clients</button>
        </div>
        <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth" style="margin-top: 5px">
            <thead>
            <tr>
                <th>Category <input type="text" x-model="category" placeholder="toys" id="category"></th>
                <th>Name</th>
                <th>Email</th>
                <th>Age<br>
                    <label> filter in range <input type="checkbox" id="agerannge" name="agerange"
                                                   x-on:change="ageRange=!ageRange">
                    </label><br>
                    <label x-show="!ageRange">fixed <input type="text" x-model="age" placeholder="25" size="3" id="age"
                                                           name="age"></label>
                    <label x-show="ageRange">range
                        <input type="text" size="3" id="ageLow" x-model="ageLow">
                        <input type="text" size="3" id="ageHigh" x-model="ageHigh">
                    </label>
                </th>
                <th>Gender
                    <select id="gender" name="gender" x-model="gender">
                        <option selected value="">Any</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </th>
                <th>Birth_date <input type="date" x-model="birth_date" id="birth_date" name="birth_date"></th>
                <!-- Add more columns as needed -->
            </tr>
            </thead>
            <tbody>
            <template x-for="item in items" :key="item.id">
                <tr>
                    <td x-text="item.category"></td>
                    <td x-text="item.firstname">John Doe</td>
                    <td x-text="item.email">john@example.com</td>
                    <td x-text="item.age">32</td>
                    <td x-text="item.gender">female</td>
                    <td x-text="item.birth_date">19.11.1997</td>
                </tr>
            </template>
            </tbody>
        </table>
        <a :href="csv" href="csv.php">download filtered data as csv</a>
    </div>
</section>
</body>
</html>
