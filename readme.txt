requirements: for local deploy this application needs docker and docker-compose

for first time you need to run from application directory:
    docker-compose up --build

for regular launch you need to start from application directory:
    docker-compose up

after containers runned application is availeble on aaddress http://localhost:8080

if you edit settings in .env file, you also need to change constants in src/config.php

if you want reread data from csv-file you need to

    run sql-script
        DROP TABLE IF EXISTS clients;

        CREATE TABLE clients
        (
            id         INT AUTO_INCREMENT PRIMARY KEY,
            category   VARCHAR(50),
            firstname  VARCHAR(255) NOT NULL,
            lastname   VARCHAR(255) NOT NULL,
            email      VARCHAR(255),
            gender     ENUM ('male','female','other'),
            birth_date  DATE,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );

        CREATE INDEX category ON clients(category);
        CREATE INDEX birth_date ON clients(birth_date);

    run script src/csvreader.php inside docker container